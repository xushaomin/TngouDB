package net.tngou.db.manage;

import java.lang.reflect.InvocationTargetException;

import net.tngou.db.entity.Page;
import net.tngou.db.entity.Request;
import net.tngou.db.lucene.LuceneManage;
import net.tngou.db.util.ResultSet;



public class SQL {

	@SuppressWarnings("rawtypes")
	private static Class[] NO_ARGS_CLASS = new Class[0];
	private static Object[] NO_ARGS_OBJECT = new Object[0];
	protected Request request =null;
	protected ResultSet response = new ResultSet();
	protected LuceneManage luceneManage = LuceneManage.getInstance();
	public ResultSet execute(Request request) 
	{
			
		this.request =request;
			try {
				ResultSet r = (ResultSet) this.getClass().getMethod(this.request.getAction(), NO_ARGS_CLASS).invoke(this, NO_ARGS_OBJECT);
				return r;
			} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | NoSuchMethodException
							| SecurityException e) {
				return new ResultSet();
			}
				
	};
}
